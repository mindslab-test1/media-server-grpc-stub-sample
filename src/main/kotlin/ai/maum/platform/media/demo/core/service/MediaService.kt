package ai.maum.platform.media.demo.core.service

import org.springframework.web.multipart.MultipartFile

interface MediaService {
    fun saveContent(file: MultipartFile)
}
