package ai.maum.platform.media.demo.infra.service

import ai.maum.aics.cdn.protobuf.MediaServerGrpc
import ai.maum.aics.cdn.protobuf.SaveContentRequest
import ai.maum.aics.cdn.protobuf.SaveContentResponse
import ai.maum.platform.media.demo.core.service.MediaService
import com.google.protobuf.ByteString
import io.grpc.ManagedChannelBuilder
import io.grpc.netty.GrpcSslContexts
import io.grpc.netty.NegotiationType
import io.grpc.netty.NettyChannelBuilder
import io.grpc.stub.StreamObserver
import io.netty.handler.ssl.SslContextBuilder
import io.netty.handler.ssl.SslProvider
import io.netty.handler.ssl.util.InsecureTrustManagerFactory
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.io.ByteArrayInputStream
import java.nio.charset.StandardCharsets

@Service
class MediaServiceImpl(
    @Value("\${maum-api.media.host}")
    mediaServiceHost: String,
    @Value("\${maum-api.media.port}")
    mediaServicePort: Int,
    @Value("\${upload-dir:/aics/api/test}")
    private val uploadDirectory: String
) : MediaService {
    private val mediaGrpcChannel = NettyChannelBuilder.forAddress(mediaServiceHost, mediaServicePort)
            .sslContext(GrpcSslContexts.forClient().trustManager(InsecureTrustManagerFactory.INSTANCE).build())
            .build()
    private val mediaStub = MediaServerGrpc.newStub(mediaGrpcChannel)
    private val logger = LoggerFactory.getLogger(this.javaClass)

    init {
        logger.info("media grpc target url($mediaServiceHost)")
    }

    override fun saveContent(file: MultipartFile) {
        val mediaBuilder = SaveContentRequest.newBuilder()
            .setUploadDirectory(uploadDirectory)
            .setContentType(file.contentType)
            .setOriginalFilename(file.originalFilename)

        val responseObserver = mediaStub.uploadContent(object : StreamObserver<SaveContentResponse> {
            override fun onNext(value: SaveContentResponse) {
                logger.info(String(value.fileDownloadUri.toByteArray(), StandardCharsets.UTF_8))
            }

            override fun onError(t: Throwable?) {
                logger.error("Err from responseObserver in media", t)
            }

            override fun onCompleted() {
                logger.info("Finished from responseObserver in media")
            }
        })

        try {
            ByteArrayInputStream(file.bytes).use { byteArrayInputStream: ByteArrayInputStream ->
                var len: Int
                val buffer = ByteArray(1024 * 512) // data unit: kb
                while (byteArrayInputStream.read(buffer).also { len = it } > 0) {
                    if (buffer.size > len) { // 파일 마지막의 남은 크기가 기준보다 작으면
                        val inputByteString = ByteString.copyFrom(ByteArray(len))
                        responseObserver.onNext(mediaBuilder.setMediaContent(inputByteString).build())
                    } else {
                        val inputByteString = ByteString.copyFrom(buffer)
                        responseObserver.onNext(mediaBuilder.setMediaContent(inputByteString).build())
                    }
                }
            }
        } catch (e: Exception) {
            responseObserver.onError(e)
            throw e
        }

        responseObserver.onCompleted()
    }
}
