package ai.maum.platform.media.demo.boundaries

import ai.maum.platform.media.demo.core.service.MediaService
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile

@RestController
@RequestMapping("/test")
class TestController (
    private val mediaService: MediaService
) {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    @PostMapping("/file:upload")
    fun upload(@RequestParam("file") file: MultipartFile) {
        mediaService.saveContent(file)
        logger.info("Success upload ${file.originalFilename}")
    }
}
