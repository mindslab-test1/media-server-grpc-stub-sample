# Media Server gRpc Client Sample

Java, Kotlin 기준 gRpc 프로토콜을 이용한 SSL적용된 클라이언트 샘플 프로젝트입니다.

## Hot to setting grpc client

1. gradle setting

```kotlin
plugins {
    ...
    id("com.google.protobuf") version "0.8.13"
    ...
}

...

val grpcVersion = "1.30.0"
val protocVersion = "3.13.0"

sourceSets {
    main {
        java {
            srcDir("src/main/protoGen")
        }
    }
}

dependencies {
    ...    
    // grpc
    implementation("io.grpc:grpc-services")
    implementation("io.grpc:grpc-netty")
    implementation("io.github.lognet:grpc-spring-boot-starter:3.5.5")
    ...       
}

protobuf {
    protoc { artifact = "com.google.protobuf:protoc:$protocVersion" }
    plugins {
        id("grpc") { artifact = "io.grpc:protoc-gen-grpc-java:$grpcVersion" }
    }
    generateProtoTasks {
        ofSourceSet("main").forEach { task ->
            task.plugins {
                id("grpc") {
                    outputSubDir = "protoGen"
                }
            }
        }
    }
    generatedFilesBaseDir = "$projectDir/src/"
}
```

2. proto 파일 추가 및 generateProto 실행

```shell script
$ ./gradlew clean generateProto
```

> IDEA상에서 맨 오른쪽 패널중 Gradle 내에서 Tasks > other 안에 "generateProto"를 클릭하여 실행할 수 있습니다.

3. gRpc Client Stub code 구성

```kotlin
val mediaGrpcChannel = NettyChannelBuilder.forAddress(mediaServiceHost, mediaServicePort)
            .sslContext(GrpcSslContexts.forClient().trustManager(InsecureTrustManagerFactory.INSTANCE).build())
            .build()

val mediaStub = MediaServerGrpc.newStub(mediaGrpcChannel)

mediaStub.uploadContent(...)
```

> 위 코드는 Java/Kotlin언어에서 gRpc SSL 연결을 위한 핵심적인 연결부분만 표현하였습니다.
> 자세한 코드 흐름은 MediaServiceImpl 클래스를 참고하시면 됩니다.

