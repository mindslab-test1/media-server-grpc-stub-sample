import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import com.google.protobuf.gradle.*

plugins {
    id("org.springframework.boot") version "2.4.3"
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
    id("com.google.protobuf") version "0.8.13"

    kotlin("jvm") version "1.4.30"
    kotlin("plugin.spring") version "1.4.30"
}

group = "ai.maum.platform.media"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

val grpcVersion = "1.30.0"
val protocVersion = "3.13.0"

repositories {
    mavenCentral()
}

sourceSets {
    main {
        java {
            srcDir("src/main/protoGen")
        }
    }
}

dependencies {
    // spring
    implementation("org.springframework.boot:spring-boot-starter-web")

    // grpc
    implementation("io.grpc:grpc-services")
    implementation("io.grpc:grpc-netty")
    implementation("io.github.lognet:grpc-spring-boot-starter:3.5.5")

    // kotlin
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    // test
    testImplementation("org.springframework.boot:spring-boot-starter-test")
}

protobuf {
    protoc { artifact = "com.google.protobuf:protoc:$protocVersion" }
    plugins {
        id("grpc") { artifact = "io.grpc:protoc-gen-grpc-java:$grpcVersion" }
    }
    generateProtoTasks {
        ofSourceSet("main").forEach { task ->
            //            task.builtins {
//                id("java") {
//                    outputSubDir = "protoGen"
//                }
//            }
            task.plugins {
                id("grpc") {
                    outputSubDir = "protoGen"
                }
            }
        }
    }
    generatedFilesBaseDir = "$projectDir/src/"
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "11"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}
